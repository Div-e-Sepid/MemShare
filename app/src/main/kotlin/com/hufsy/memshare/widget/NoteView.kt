/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.navigation.findNavController
import com.google.android.material.card.MaterialCardView
import com.hufsy.memshare.R
import com.hufsy.memshare.common.model.Model
import com.hufsy.memshare.ui.preview.PreviewFragmentArgs
import com.hufsy.memshare.util.getColorCompat
import com.hufsy.memshare.util.getDimension
import com.hufsy.memshare.util.getDrawableCompat
import com.hufsy.memshare.util.showImage
import kotlinx.android.synthetic.main.view_note.view.*

class NoteView(context: Context, attrs: AttributeSet? = null) : MaterialCardView(context, attrs) {

    init {
        inflate(context, R.layout.view_note, this)

        layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        val margin = context.getDimension(R.dimen.extraSmall)
        (layoutParams as LinearLayout.LayoutParams).setMargins(margin, margin, margin, margin)

        setCardBackgroundColor(context.getColorCompat(R.color.background))
        foreground = context.getDrawableCompat(R.drawable.touchable_background)
    }

    fun setNote(note: Model.Note?) {
        if (note != null) {
            titleTextView.text = note.title
            descriptionTextView.text = note.description
            titleTextView.setBackgroundColor(context.getColorCompat(R.color.transparentWhite))
            descriptionTextView.setBackgroundColor(context.getColorCompat(R.color.transparentWhite))
            imageView.showImage(note.imageName)
            setOnClickListener {
                val arguments = PreviewFragmentArgs.Builder(note.id.toString(), note.title, note.imageName).build().toBundle()
                findNavController().navigate(R.id.previewFragment, arguments)
            }
        } else {
            titleTextView.setBackgroundColor(context.getColorCompat(R.color.windowBackground))
            descriptionTextView.setBackgroundColor(context.getColorCompat(R.color.windowBackground))
        }
    }
}