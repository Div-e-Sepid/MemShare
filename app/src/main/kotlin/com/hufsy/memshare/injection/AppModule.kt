/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.injection

import android.content.Context
import androidx.room.Room
import com.hufsy.memshare.base.BaseApplication
import com.hufsy.memshare.business.file.FileHelper
import com.hufsy.memshare.business.file.IFileHelper
import com.hufsy.memshare.business.persistance.NoteDatabaseHelper
import com.hufsy.memshare.business.persistance.domain.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideContext(app: BaseApplication): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase = Room.databaseBuilder(context, AppDatabase::class.java, "database.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideNoteDatabaseHelper(database: AppDatabase): NoteDatabaseHelper = NoteDatabaseHelper(database.noteDao())

    @Provides
    @Singleton
    fun provideFileHelper(context: Context): IFileHelper = FileHelper(context)
}