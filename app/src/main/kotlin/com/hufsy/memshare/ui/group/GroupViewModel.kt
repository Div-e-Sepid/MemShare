/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.group

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.business.persistance.NoteDatabaseHelper
import javax.inject.Inject

class GroupViewModel @Inject constructor(private val noteDatabaseHelper: NoteDatabaseHelper) : ViewModel() {

    val archived = noteDatabaseHelper.fetchPaged(true)

    val marked = noteDatabaseHelper.fetchMarkedPaged()
}