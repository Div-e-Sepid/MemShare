/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseFragment
import com.hufsy.memshare.ui.MainActivity
import com.hufsy.memshare.util.getInteger
import com.hufsy.memshare.util.gone
import com.hufsy.memshare.util.visible
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_group.*
import javax.inject.Inject

class GroupFragment : BaseFragment() {

    companion object {
        const val ARCHIVED = "ARCHIVED"
        const val MARKED = "MARKED"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: GroupViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(GroupViewModel::class.java)
    }

    private val type: String by lazy {
        GroupFragmentArgs.fromBundle(arguments).type
    }

    private val listAdapter = GroupListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View = inflater.inflate(R.layout.fragment_group, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        groupRecyclerView.layoutManager = StaggeredGridLayoutManager(context?.getInteger(R.integer.span) ?: 2, VERTICAL)
        groupRecyclerView.adapter = listAdapter

        showNotes()
    }

    private fun showNotes() {
        val groupLiveData = when (type) {
            ARCHIVED -> {
                (activity as MainActivity).toolbar.title = getString(R.string.labelArchive)
                viewModel.archived
            }
            MARKED -> {
                (activity as MainActivity).toolbar.title = getString(R.string.labelMark)
                viewModel.marked
            }
            else -> {
                throw IllegalArgumentException("Type is not supported")
            }
        }
        groupLiveData.observe(this, Observer {
            listAdapter.submitList(it)
            if (it.isEmpty()) {
                emptyTextView.visible()
            } else {
                emptyTextView.gone()
            }
        })
    }
}