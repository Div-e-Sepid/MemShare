/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.group

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.hufsy.memshare.common.model.Model
import com.hufsy.memshare.widget.NoteView

class GroupListAdapter : PagedListAdapter<Model.Note, GroupListAdapter.NoteViewHolder>(object : DiffUtil.ItemCallback<Model.Note>() {

    override fun areItemsTheSame(oldItem: Model.Note, newItem: Model.Note): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Model.Note, newItem: Model.Note): Boolean = oldItem == newItem
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NoteViewHolder(NoteView(parent.context))

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) = holder.noteView.setNote(getItem(position))

    inner class NoteViewHolder(val noteView: NoteView) : RecyclerView.ViewHolder(noteView)
}