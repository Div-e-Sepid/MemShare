/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui

import com.hufsy.memshare.ui.about.AboutBuilder
import com.hufsy.memshare.ui.group.GroupBuilder
import com.hufsy.memshare.ui.home.HomeBuilder
import com.hufsy.memshare.ui.note.NoteBuilder
import com.hufsy.memshare.ui.preview.PreviewBuilder
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainBuilder {

    @ContributesAndroidInjector(modules = [HomeBuilder::class, NoteBuilder::class, PreviewBuilder::class, GroupBuilder::class, AboutBuilder::class])
    internal abstract fun mainActivity(): MainActivity
}