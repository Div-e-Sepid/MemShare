/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui

import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseActivity
import com.hufsy.memshare.ui.group.GroupFragment
import com.hufsy.memshare.ui.group.GroupFragmentArgs
import com.hufsy.memshare.ui.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val navigationController: NavController by lazy {
        findNavController(R.id.mainHostFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initActionBar()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp() = navigationController.navigateUp()

    override fun onNavigationItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.archive -> {
            val arguments = GroupFragmentArgs.Builder(GroupFragment.ARCHIVED).build().toBundle()
            navigationController.navigate(R.id.groupFragment, arguments)
            drawerLayout.closeDrawer(Gravity.START)
            true
        }
        R.id.mark -> {
            val arguments = GroupFragmentArgs.Builder(GroupFragment.MARKED).build().toBundle()
            navigationController.navigate(R.id.groupFragment, arguments)
            drawerLayout.closeDrawer(Gravity.START)
            true
        }
        R.id.about -> {
            navigationController.navigate(R.id.aboutFragment)
            drawerLayout.closeDrawer(Gravity.START)
            true
        }
        else -> {
            false
        }
    }

    fun initActionBar() {
        setSupportActionBar(toolbar)
        setupActionBarWithNavController(this, navigationController, drawerLayout)
        setupWithNavController(navigationView, navigationController)

        navigationView.setNavigationItemSelectedListener(this)

        toolbar.setNavigationOnClickListener {
            if ((navigationController.currentDestination as FragmentNavigator.Destination).fragmentClass == HomeFragment::class.java && !drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.openDrawer(Gravity.START)
            } else {
                onBackPressed()
            }
        }
    }
}