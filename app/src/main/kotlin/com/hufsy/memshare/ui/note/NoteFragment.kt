/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.note

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseFragment
import com.hufsy.memshare.common.model.Model
import com.hufsy.memshare.dialog.ChoosImageDialogHelper
import com.hufsy.memshare.util.hideSoftKeyboard
import com.hufsy.memshare.util.isPermissionGranted
import com.hufsy.memshare.util.showImage
import kotlinx.android.synthetic.main.fragment_note.*
import java.io.File
import javax.inject.Inject

class NoteFragment : BaseFragment(), View.OnClickListener {

    companion object {

        private const val URI = "URI"
        private const val CAMERA = 1
        private const val STORAGE = 2
    }

    private var takenPhotoURI: Uri? = null

    private var imageName: String? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: NoteViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(NoteViewModel::class.java)
    }

    private val id: Long? by lazy {
        NoteFragmentArgs.fromBundle(arguments).id?.toLong()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageView.setOnClickListener(this)

        savedInstanceState?.getString(URI)?.let {
            takenPhotoURI = Uri.parse(it)
            imageView.showImage(takenPhotoURI)
        }

        id?.let {
            showNote(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.note, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.done -> {
            titleTextInputEditText.hideSoftKeyboard()
            saveNote()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA -> {
                if (grantResults[0].isPermissionGranted()) {
                    tackImage()
                }
            }
            STORAGE -> {
                if (grantResults[0].isPermissionGranted()) {
                    chooseImage()
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CAMERA -> {
                if (resultCode == RESULT_OK) {
                    imageView.showImage(takenPhotoURI)
                }
            }
            STORAGE -> {
                if (resultCode == RESULT_OK) {
                    data?.data?.let {
                        takenPhotoURI = it
                        imageView.showImage(takenPhotoURI)
                    }
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(URI, takenPhotoURI?.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onClick(view: View) {
        ChoosImageDialogHelper(view.context, this).show {
            when (it) {
                ChoosImageDialogHelper.Choice.CAMERA -> {
                    openCamera()
                }
                ChoosImageDialogHelper.Choice.GALLERY -> {
                    openGallery()
                }
            }
        }
    }

    private fun openCamera() {
        context?.let {
            if (it.isPermissionGranted(Manifest.permission.CAMERA)) {
                tackImage()
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA)
            }
        }
    }

    private fun openGallery() {
        context?.let {
            if (it.isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                chooseImage()
            } else {
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE)
            }
        }
    }

    private fun tackImage() {
        context?.let {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(it.packageManager) != null) {
                val photoFile: File = viewModel.createImageFile()
                takenPhotoURI = FileProvider.getUriForFile(it, getString(R.string.AUTHORITY), photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, takenPhotoURI)
                takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                startActivityForResult(takePictureIntent, CAMERA)
            }
        }
    }

    private fun chooseImage() {
        context?.let { context ->
            val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            pickPhoto.resolveActivity(context.packageManager)?.let {
                startActivityForResult(pickPhoto, STORAGE)
            }
        }
    }

    private fun showNote(id: Long) {
        viewModel.getNote(id).observe(this, Observer {
            it?.let { note ->
                titleTextInputEditText.setText(note.title)
                descriptionTextInputEditText.setText(note.description)
                imageName = note.imageName
                if (takenPhotoURI != null) {
                    imageView.showImage(takenPhotoURI)
                } else {
                    imageView.showImage(note.imageName)
                }
            }
        })
    }

    private fun saveNote() {
        if (isInputValid()) {
            val title = titleTextInputEditText.text.toString()
            val description = descriptionTextInputEditText.text?.toString() ?: ""

            val finalImageName = if (takenPhotoURI != null) {
                takenPhotoURI?.toString()
            } else {
                imageName
            }

            val note = Model.Note(
                    id = id,
                    title = title,
                    description = description,
                    imageName = finalImageName
            )

            viewModel.persistNote(note)
            NavHostFragment.findNavController(this).popBackStack()
        }
    }

    private fun isInputValid(): Boolean {
        var valid = true

        valid = if (titleTextInputEditText.text.isNullOrEmpty()) {
            titleTextInputLayout.error = getString(R.string.errorEmptyTitle)
            false
        } else {
            titleTextInputLayout.error = null
            true
        } && valid

        return valid
    }
}