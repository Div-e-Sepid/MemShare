/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.note

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.injection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class NoteBuilder {

    @ContributesAndroidInjector
    internal abstract fun noteFragment(): NoteFragment

    @Binds
    @IntoMap
    @ViewModelKey(NoteViewModel::class)
    internal abstract fun bindNoteViewModel(viewModel: NoteViewModel): ViewModel
}