/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseFragment

class AboutFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View = inflater.inflate(R.layout.fragment_about, container, false)
}