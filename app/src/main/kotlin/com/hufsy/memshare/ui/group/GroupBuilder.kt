/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.group

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.injection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class GroupBuilder {

    @ContributesAndroidInjector
    internal abstract fun groupFragment(): GroupFragment

    @Binds
    @IntoMap
    @ViewModelKey(GroupViewModel::class)
    internal abstract fun bindGroupViewModel(viewModel: GroupViewModel): ViewModel
}