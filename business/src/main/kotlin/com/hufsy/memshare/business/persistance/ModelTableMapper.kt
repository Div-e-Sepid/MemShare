/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance

import com.hufsy.memshare.business.persistance.domain.Table
import com.hufsy.memshare.common.model.Model

fun Table.Note.map() = Model.Note(
        id = id,
        title = title,
        description = description,
        imageName = imageName,
        archived = archived,
        marked = marked
)

fun Model.Note.map() = Table.Note(
        id = id,
        title = title,
        description = description,
        imageName = imageName,
        archived = archived,
        marked = marked
)