/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

sealed class Table {

    @Entity(tableName = "Notes")
    data class Note(
            @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Long?,
            @ColumnInfo(name = "title") val title: String,
            @ColumnInfo(name = "description") val description: String,
            @ColumnInfo(name = "image_name") val imageName: String?,
            @ColumnInfo(name = "archived") val archived: Boolean,
            @ColumnInfo(name = "marked") val marked: Boolean
    ) : Table()
}